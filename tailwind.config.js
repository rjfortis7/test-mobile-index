module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    fill: theme => ({
      primary: {
        lighter: theme('colors.primary.lighter'),
      },
      secondary: {
        lighter: theme('colors.secondary.lighter'),
      }
    }),
    extend: {
      colors: {
        primary: {
          lighter: '#E6FFFA',
          light: '#81E6D9',
          default: '#319795',
          dark: '#319795'
        },
        secondary: {
          lighter: '#EEF8FD',
          default: '#3182CE',
          dark: '#3182CE'
        },
        dark: {
          light: '#718096',
          default: '#4A5568',
        }
      },
      boxShadow: {
        't-md': '0 -4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
        'b-md': '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
      },
      width: {
        'sm': '200px'
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
